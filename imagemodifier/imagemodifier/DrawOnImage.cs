﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace imagemodifier
{
    class DrawOnImage
    {
        private string imagePath = "";
        private string imageName = "";
        private string textColor = "";
        private string imageTransliedName = "";
        private bool isImageCreated = false;

        public DrawOnImage(string imagePath, string imageName, string textColor)
        {
            this.imagePath = imagePath;
            this.imageName = imageName;
            this.textColor = textColor;
            this.imageTransliedName = Translit(this.imageName);
        }
        public void createImageWithText()
        {
            char[] array = new char[1000 * 1000];
            array[0] = ' ';
            List<string> strLines;

            if (imageName.Length <= 24)
            {
                strLines = new List<string> { imageName };
                createByStringLenght(strLines, "short");
            }
            else if (imageName.Length > 24 && imageName.Length < 80)
            {
                strLines = SplitToLines(imageName, array, 30);
                createByStringLenght(strLines, "long");
            }
            else if (imageName.Length > 80 && imageName.Length < 200)
            {
                strLines = SplitToLines(imageName, array, 43);
                createByStringLenght(strLines, "verylong");
            }
        }
        public bool isCreated()
        {
            return this.isImageCreated;
        }

        private void createByStringLenght(List<string> title, string caseNum)
        {
            switch (caseNum)
            {
                case "short":
                    placeTextOnImageShort(title,this.textColor);
                    break;

                case "long":
                    placeTextOnImageLong(title, this.textColor);
                    break;

                case "verylong":
                    placeTextOnImageVeryLong(title, this.textColor);
                    break;
            }
        }
        private void placeTextOnImageShort(List<string> title, string color)
        {
            PointF firstLocation = new PointF(43f, 120f);
            Bitmap bitmap = (Bitmap)Image.FromFile(this.imagePath);
            if (color == "Black")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 12))
                    {
                        graphics.DrawString(title[0], arialFont, Brushes.Black, firstLocation);
                    }
                }
            }
            if(color == "White")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 12))
                    {
                        graphics.DrawString(title[0], arialFont, Brushes.White, firstLocation);
                    }
                }
            }
            bitmap.Save(imgDir + this.imageTransliedName + ".png");
            this.isImageCreated = isImageExists(imgDir + this.imageTransliedName + ".png");
        }
        private void placeTextOnImageLong(List<string> title, string color)
        {
            PointF firstLocation = new PointF(43f, 120f);
            PointF secondLocation = new PointF(43f, 180f);
            PointF thirdLocation = new PointF(43f, 240f);

            List<PointF> locations = new List<PointF>();
            locations.Add(firstLocation);
            locations.Add(secondLocation);
            locations.Add(thirdLocation);

            Bitmap bitmap = (Bitmap)Image.FromFile(this.imagePath);
            if (color == "Black")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 11))
                    {
                        foreach (var item in title)
                        {
                            graphics.DrawString(item, arialFont, Brushes.Black, locations[title.IndexOf(item)]);
                        }
                    }
                }
            }
            if (color == "White")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 11))
                    {
                        foreach (var item in title)
                        {
                            graphics.DrawString(item, arialFont, Brushes.White, locations[title.IndexOf(item)]);
                        }
                    }
                }
            }

            bitmap.Save(imgDir + this.imageTransliedName + ".png");
            this.isImageCreated = isImageExists(imgDir + this.imageTransliedName + ".png");
        }
        private void placeTextOnImageVeryLong(List<string> title, string color)
        {
            PointF firstLocation = new PointF(43f, 120f);
            PointF secondLocation = new PointF(43f, 180f);
            PointF thirdLocation = new PointF(43f, 240f);

            List<PointF> locations = new List<PointF>();
            locations.Add(firstLocation);
            locations.Add(secondLocation);
            locations.Add(thirdLocation);

            Bitmap bitmap = (Bitmap)Image.FromFile(this.imagePath);

            if (color == "Black")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 10))
                    {
                        foreach (var item in title)
                        {
                            graphics.DrawString(item, arialFont, Brushes.Black, locations[title.IndexOf(item)]);
                        }
                    }
                }
            }
            if (color == "White")
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Franklin Gothic Demi Cond", 10))
                    {
                        foreach (var item in title)
                        {
                            graphics.DrawString(item, arialFont, Brushes.White, locations[title.IndexOf(item)]);
                        }
                    }
                }
            }
            bitmap.Save(imgDir + this.imageTransliedName + ".png");
            this.isImageCreated = isImageExists(imgDir + this.imageTransliedName + ".png");
        }
        private List<string> SplitToLines(string text, char[] splitOnCharacters, int maxStringLength)
        {
            var sb = new StringBuilder();
            var index = 0;
            List<string> l = new List<string>();

            while (text.Length > index)
            {
                // start a new line, unless we've just started
                if (index != 0)
                    sb.AppendLine();

                // get the next substring, else the rest of the string if remainder is shorter than `maxStringLength`
                var splitAt = index + maxStringLength <= text.Length
                    ? text.Substring(index, maxStringLength).LastIndexOfAny(splitOnCharacters)
                    : text.Length - index;

                // if can't find split location, take `maxStringLength` characters
                splitAt = (splitAt == -1) ? maxStringLength : splitAt;

                // add result to collection & increment index
                sb.Append(text.Substring(index, splitAt).Trim());
                l.Add(text.Substring(index, splitAt).Trim());
                index += splitAt;
            }

            return l;
        }
        private string Translit(string str)
        {
            string[] lat_up = { "A", "B", "V", "G", "D", "E", "Yo", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "Ts", "Ch", "Sh", "Shch", "", "Y", "", "E", "Yu", "Ya", "-" };
            string[] lat_low = { "a", "b", "v", "g", "d", "e", "yo", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "ts", "ch", "sh", "shch", "", "y", "", "e", "yu", "ya", "-" };
            string[] rus_up = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я", ":" };
            string[] rus_low = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", ":" };
            for (int i = 0; i <= 33; i++)
            {
                str = str.Replace(rus_up[i], lat_up[i]);
                str = str.Replace(rus_low[i], lat_low[i]);
            }
            return str;
        }
        private bool isImageExists(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    return true;
                }

                return false;
            }
            catch { return false; }
        }

        private const string _path = "imgdir\\";
        private static string imgDir
        {
            get
            {
                string path = AppDomain.CurrentDomain.BaseDirectory;
                string datapath = path + _path;
                return datapath;
            }
        }
    }
}
