﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace imagemodifier
{
    public partial class Form1 : Form
    {
        private string imagePath = "";
        private string imageName = "";
        private string textColor = "";

        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedItem = "Black";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        OpenFileDialog ofd = new OpenFileDialog();
        private void button1_Click(object sender, EventArgs e)
        {
            ofd.Filter = "PNG|*.png";
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;
                this.imagePath = ofd.FileName;
            }   
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && this.imagePath!= "")
            {
                this.imageName = textBox2.Text;
                this.textColor = comboBox1.SelectedItem.ToString();

                DrawOnImage m = new DrawOnImage(this.imagePath, this.imageName, this.textColor);
                m.createImageWithText();
                if (m.isCreated())
                {
                    textBox1.Clear();
                    textBox2.Clear();
                    MessageBox.Show("Image successfully created");
                }
                else {
                    MessageBox.Show("Image NOT created, something went wrong... Recheck settings");
                    textBox1.Clear();
                    textBox2.Clear();
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (ofd.FileName == "")
            {
                MessageBox.Show("Please select Image Template first!");
                textBox2.Clear();
            }
            else
                button2.Visible = true;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
